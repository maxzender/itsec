Bundler installieren: gem install bundler
Installieren der Gems mit Bundler: bundle install --path=vendor/bundle
Gem mit Bundler ausführen: bundle exec <Gemname>

Sequel Migrations ausführen: bundle exec sequel -m migrations/ sqlite://database.sqlite
Sequel Migrations rückgänig machen: bundle exec sequel -m migrations/ -M 0 sqlite://database.sqlite

Server starten: bundle exec ruby app.rb

Zugangsdaten für Nicht-Admin-Nutzer:
E-Mail: user@haw-hamburg.de
Passwort: user

Zugangsdaten für Admin-Nutzer:
E-Mail: admin@haw-hamburg.de
Passwort: admin
