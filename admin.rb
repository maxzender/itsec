require 'sinatra/base'
require './models/user'
require './services/user_service'

class Admin < Sinatra::Base
  register Sinatra::Flash

  before '/admin*' do
    unless current_user.is_admin?
      flash[:error] = "Sie sind kein Administrator."
      redirect '/'
    end
  end

  get '/admin' do
    erb :admin
  end

  get '/admin/create_user' do
    erb :admin_create_user
  end

  post '/admin/create_user' do
    begin
      user = UserService.create(
        :email => params['email'],
        :password => params['password']
      )

      flash[:info] = "Nutzer wurde erstellt. Bestätigungs-E-Mail wurde versandt."
      redirect "/confirmation_email/#{user.id}"
    rescue UserService::FailedValidationException => e
      flash[:error] = e.message
      redirect '/admin/create_user'
    end
  end

  get '/admin/users' do
    erb :admin_users, :locals => { :users => User.all }
  end

  get '/admin/change_email/:id' do
    erb :admin_change_email, :locals => { :user => User[params['id']] }
  end

  post '/admin/change_email' do
    begin
      user = User[params['id']]
      UserService.update_email(user, params['email'])

      flash[:info] = "Eine Bestätigungs-E-Mail wurde versandt."
      redirect "/confirmation_email/#{user.id}"
    rescue UserService::FailedValidationException => e
      flash[:error] = e.message
      redirect "/admin/change_email/#{params['id']}"
    end
  end

  get '/admin/change_password/:id' do
    erb :admin_change_password, :locals => { :user => User[params['id']] }
  end

  post '/admin/change_password' do
    user = User[params['id']]
    UserService.update_password(user, params['password'])

    flash[:info] = "Passwort wurde geändert."
    redirect '/admin'
  end

  get '/admin/delete/:id' do
    user = User[params['id']]
    user.destroy

    flash[:info] = "Nutzer wurde gelöscht."
    redirect '/admin'
  end
end
