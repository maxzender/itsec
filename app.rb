require 'sinatra'
require 'sinatra/flash'
require 'tilt/erb'
require './login'
require './admin'
require './models/user'
require './services/user_service.rb'
require './services/email_confirmation_service.rb'
require './services/password_recovery_service.rb'

use Login
use Admin

def current_user
  User.find(:email => session['email'])
end

before do
  # no authentication required for email confirmation
  pass if %w[confirm_email reset_password password_email reset_password_final].include? request.path_info.split('/')[1]
  unless session['email']
    redirect '/login'
  end
end


get '/' do
  erb :home, :locals => { :user => current_user }
end

get '/logout' do
  session.clear
  flash[:info] = "Sie sind jetzt ausgeloggt."
  redirect '/login'
end

get '/change_email' do
  erb :change_email
end

post '/change_email' do
  begin
    user = current_user
    UserService.update_email(user, params['email'])
    session['email'] = user.email

    flash[:info] = "Eine Bestätigungs-E-Mail wurde versandt."
    redirect "/confirmation_email/#{user.id}"
  rescue UserService::FailedValidationException => e
    flash[:error] = e.message
    redirect '/change_email'
  end
end

get '/confirmation_email/:id' do
  user = User[params['id']]
  erb :confirmation_email, :locals => { :user => user }
end

get '/confirm_email/:key' do
  if EmailConfirmationService.verify_confirmation(params['key'])
    flash[:info] = "E-Mail wurde erfolreich bestätigt."
  else
    flash[:error] = "Der Link ist ungültig."
  end

  redirect '/'
end

get '/reset_password' do
  erb :reset_password, :locals => { :user => current_user }
end

post '/reset_password' do
  begin
    PasswordRecoveryService.generate_password_confirmation(params['email'])
    user = User.find(:email => params['email'])

    flash[:info] = "Eine E-Mail zur Änderung des Passworts wurde versandt."
    redirect "/password_email/#{user.id}"
  rescue RuntimeError => e
    flash[:error] = e.message
    redirect '/reset_password'
  end
end

get '/reset_password/:key' do
  if PasswordRecoveryService.verify_reset_request(params['key'])
    user = User.find(:password_confirmation_key => params['key'])
    erb :reset_password_form, :locals => { :user => user }
  else
    flash[:error] = "Der Link ist ungültig."
    redirect '/login'
  end
end

get '/password_email/:id' do
  user = User[params['id']]
  erb :password_email, :locals => { :user => user }
end

post '/reset_password_final' do
  if params['password'] == params['password_confirmation']
    if PasswordRecoveryService.reset_password(params['key'], params['password'])
      flash[:info] = "Neues Passwort wurde gespeichert. Sie können sich jetzt einloggen"
    else
      flash[:error] = "Ungültige Anfrage."
    end

    redirect '/'
  else
    flash[:error] = "Passwörter stimmen nicht überein."
    redirect "/reset_password/#{params['key']}"
  end
end

get '/change_password' do
  erb :change_password
end

post '/change_password' do
  if params['password'] == params['password_confirmation']
    UserService.update_password(current_user, params['password'])

    flash[:info] = "Passwort wurde erfolgreich geändert."
    redirect '/'
  else
    flash[:error] = "Passwörter stimmen nicht überein."
    redirect '/change_password'
  end
end
