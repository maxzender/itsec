require 'sinatra/base'
require './models/user'
require './services/user_service'

class Login < Sinatra::Base
  enable :sessions
  set :session_secret, '02ee9c2fba5060b8ffe7a58c9afddb8cc80a01ff'
  register Sinatra::Flash

  get '/login' do
    erb :login # Rendert views/login.erb
  end

  post '/login' do
    user = UserService.authenticate(params['email'], params['password'])
    if user && !user.is_new?
      session['email'] = params['email']
      flash[:info] = "Sie sind jetzt eingeloggt."
      redirect '/'
    elsif user && user.is_new?
      flash[:error] = "Sie haben Ihre E-Mail-Adresse nicht bestätigt."
      redirect '/login'
    else
      flash[:error] = "Falsche Nutzer/Passwort-Kombination."
      redirect '/login'
    end
  end
end
