Sequel.migration do
  change do
    add_column :users, :admin, TrueClass, :default => false
  end
end
