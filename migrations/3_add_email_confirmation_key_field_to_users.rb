Sequel.migration do
  change do
    add_column :users, :email_confirmation_key, String
    add_column :users, :email_confirmation_creation, DateTime
    add_column :users, :is_new, TrueClass
  end
end
