Sequel.migration do
  change do
    add_column :users, :password_confirmation_key, String
    add_column :users, :password_confirmation_creation, DateTime
    add_column :users, :password_was_reset, TrueClass
  end
end
