require 'digest'
require 'sequel'
DB = Sequel.connect('sqlite://database.sqlite')

class User < Sequel::Model
  SALT_LENGTH = 10

  def password=(password)
    self.salt = random_salt if self.salt.nil?
    super(hash_password(password))
  end

  def validate
    super
    errors.add(:email, "existiert bereits") if email_exists
  end

  def is_admin?
    self.admin
  end

  def is_new?
    self.is_new
  end

  def email_exists
    user = User.find(:email => email)
    !user.nil? && user.id != self.id
  end

  def hash_password(password)
    Digest::SHA512.hexdigest(self.salt + password)
  end

  def random_salt
    SecureRandom.hex(User::SALT_LENGTH)
  end

end
