require 'date'
require 'securerandom'
require './models/user'

class EmailConfirmationService
  KEY_LENGTH = 20
  KEY_VALIDITY_PERIOD = 30 # in minutes

  def self.generate_confirmation(user)
    user.email_confirmation_key = EmailConfirmationService.generate_random_key
    user.email_confirmation_creation = DateTime.now
    user.save
  end

  def self.verify_confirmation(key)
    user = User.find(:email_confirmation_key => key)
    if user && EmailConfirmationService.key_is_valid?(user)
      user.email_confirmation_key = nil
      user.email_confirmation_creation = nil
      user.is_new = false
      user.save
      true
    else
      false
    end
  end

  def self.key_is_valid?(user)
    creation_time = user.email_confirmation_creation
    if creation_time
      ((Time.now - creation_time)/60).round < EmailConfirmationService::KEY_VALIDITY_PERIOD
    else
      false
    end
  end

  def self.generate_random_key
    SecureRandom.hex(EmailConfirmationService::KEY_LENGTH)
  end
end
