require './models/user'

class PasswordRecoveryService
  UnconfirmedEmailException = Class.new(RuntimeError)
  UserNotFoundException = Class.new(RuntimeError)

  KEY_LENGTH = 20
  KEY_VALIDITY_PERIOD = 30 # in minutes

  def self.generate_password_confirmation(email)
    user = User.find(:email => email)

    if user && !user.email_confirmation_key
      user.password_confirmation_key = generate_random_key
      user.password_confirmation_creation = Time.now
      user.save
    elsif user && user.email_confirmation_key
      raise UnconfirmedEmailException, "Die E-Mail-Adresse wurde noch nicht bestätigt."
    else
      raise UserNotFoundException, "Es wurde kein Benutzer mit der geg. Mail-Adresse gefunden."
    end
  end

  def self.reset_password(key, password)
    user = User.find(:password_confirmation_key => key)
    if verify_reset_request(key)
      user.password_confirmation_key = nil
      user.password_confirmation_creation = nil
      user.password_was_reset = true
      user.password = password
      user.save
      true
    else
      false
    end
  end

  def self.verify_reset_request(key)
    user = User.find(:password_confirmation_key => key)
    user && key_is_valid?(user)
  end

  def self.key_is_valid?(user)
    creation_time = user.password_confirmation_creation
    if creation_time
      ((Time.now - creation_time)/60).round < PasswordRecoveryService::KEY_VALIDITY_PERIOD
    else
      false
    end
  end

  def self.generate_random_key
    SecureRandom.hex(EmailConfirmationService::KEY_LENGTH)
  end
end
