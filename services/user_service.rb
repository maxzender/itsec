require './services/email_confirmation_service'

class UserService
  FailedValidationException = Class.new(RuntimeError)

  def self.create(attributes)
    begin
      new_user_attributes = attributes.merge({:is_new => true})
      user = User.create(new_user_attributes)
      EmailConfirmationService.generate_confirmation(user)
    rescue Sequel::ValidationFailed => e
      raise FailedValidationException, e.message
    end
  end

  def self.update_password(user, password)
    begin
      user.password = password
      user.password_was_reset = nil
      user.save
    rescue Sequel::ValidationFailed => e
      raise FailedValidationException, e.message
    end
  end

  def self.update_email(user, email)
    begin
      user.email = email
      user.save
      EmailConfirmationService.generate_confirmation(user)
    rescue Sequel::ValidationFailed => e
      raise FailedValidationException, e.message
    end
  end

  def self.authenticate(email, password)
    user = User.find(:email => email)
    if user && user.password == user.hash_password(password)
      user
    else
      nil
    end
  end
end
